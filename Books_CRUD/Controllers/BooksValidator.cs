﻿using Domain.Models;
using FluentValidation;
using System.Drawing;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Books_CRUD.Controllers
{
    public class BooksValidator : AbstractValidator<Books>
    {
        public BooksValidator()
        {
            Regex regEx = new Regex("^[a-zA-Z].");

            RuleFor(model => model.BookId).NotNull().GreaterThanOrEqualTo(100).WithMessage("BookId should be start from 100"); ; ;
            RuleFor(model => model.UserId).NotNull().GreaterThanOrEqualTo(0);
            RuleFor(model => model.AuthorName).NotNull().Matches(regEx).WithMessage("Name should contain alphabets only");
            RuleFor(model => model.BookName).NotNull();
            RuleFor(model => model.Price).NotNull();
            



        }
    }
}
