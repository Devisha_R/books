﻿using CQRS.Commands;
using CQRS.Queries;
using Domain.Models;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Books_CRUD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private IMediator mediator;

        public BooksController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet("GetAll")]
        public async Task<IEnumerable<Books>> GetAll()
        {
            return await mediator.Send(new GetAllBooksQuery());
        }


        [HttpGet("GetBookById")]
        public async Task<Books> GetBookById(int Id)
        {
            return await mediator.Send(new GetBookByIdQuery(Id));
        }

        [HttpPost("AddBook")]
        public async Task<bool> Add(Books book)
        {       
          await mediator.Send(new AddBooksCommand(book));
          return true;

        }

        [HttpDelete("DeleteBook")]
        public async Task<bool> Delete(int Id)
        {
            await mediator.Send(new DeleteBookCommand (Id));
            return true;
        }

        [HttpPut("UpdateBook")]
        public async Task<bool> Update(Books book)
        {
            await mediator.Send(new UpdateBookCommand (book));
            return true;
        }

    }
}
