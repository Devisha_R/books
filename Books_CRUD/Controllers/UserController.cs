﻿using CQRS.Commands;
using CQRS.Queries;
using Domain.Models;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Books_CRUD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IMediator mediator;

        public UserController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet("GetAll")]
        public async Task<IEnumerable<User>> GetAll()
        {
            return await mediator.Send(new GetAllUsersQuery());
        }


        [HttpGet("GetUserById")]
        public async Task<User> GetUserById(int Id)
        {
            return await mediator.Send(new GetUserByIdQuery (Id));
        }

        [HttpPost("AddUser")]
        public async Task<bool> Add(User user)
        {
            return await mediator.Send(new AddUserCommand (user));
        }

        [HttpDelete("DeleteUser")]
        public async Task<bool> Delete(int Id)
        {
            await mediator.Send(new DeleteUserCommand  (Id));
            return true;
        }

        [HttpPut("UpdateUser")]
        public async Task<bool> Update(User user)
        {
            await mediator.Send(new UpdateUserCommand (user));
            return true;
        }

    }
}

