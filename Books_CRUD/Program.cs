using Books_CRUD.Controllers;
using CQRS;
using DataAccess;
using DataAccess.Interface;
using DataAccess.Repository;
using FluentValidation.AspNetCore;
using MediatR;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddTransient<IGeneric, GenericRepo>();
builder.Services.AddTransient<IBooks, BooksRepo>();
builder.Services.AddTransient<IUser,UserRepo>();
builder.Services.AddMediatR(typeof(EntryPoint).Assembly);
builder.Services.Configure<ConnectionStrings>(builder.Configuration.GetSection("ConnectionStrings"));
builder.Services.AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<BooksValidator>());
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
