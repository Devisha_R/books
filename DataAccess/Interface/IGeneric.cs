﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Interface
{
    public interface IGeneric
    {
        Task<IEnumerable<T>> LoadData<T, U>(string sqlQuery, U parameters, string connectionId = "default");
        Task SaveData<T, U>(string sqlquery, U parameters, string connectionid = "default");
    }
}
