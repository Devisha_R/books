﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Interface
{
   public interface IBooks
    {
        Task<Books> GetBookById(int id);
        Task<IEnumerable<Books>> GetAllBooks();
        Task<Books> AddBook(Books book); // Changed Return Type
        Task<bool> DeleteBook(int id);
        Task<bool> UpdateBook(Books book);
    }
}
