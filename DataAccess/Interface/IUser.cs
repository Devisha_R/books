﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Interface
{
    public interface IUser
    {
        Task<User> GetUserById(int id);
        Task<IEnumerable<User>> GetAllUser();
        Task<bool> AddUser(User user);
        Task<bool> DeleteUser(int id);
        Task<bool> UpdateUser(User user);
    }
}
