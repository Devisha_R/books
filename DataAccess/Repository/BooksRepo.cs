﻿using DataAccess.Interface;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class BooksRepo : IBooks
    {
        private readonly IGeneric igeneric;

        public BooksRepo(IGeneric igeneric)
        {
            this.igeneric = igeneric;
        }

        public async Task<Books> AddBook(Books book)
        {
            await igeneric.SaveData<Books, dynamic>("INSERT INTO dbo.Books (BookId,UserId,BookName,AuthorName,Price) VALUES (@BookId,@UserId,@BookName,@AuthorName,@Price)",
            new { book.BookId, book.UserId, book.BookName ,book.AuthorName,book.Price});
            return book;
        }

        public async Task<bool> DeleteBook(int id)
        {
            await igeneric.SaveData<Books, dynamic>("DELETE FROM dbo.Books WHERE BookId=@id", new { id });
            return true;
        }

        public Task<IEnumerable<Books>> GetAllBooks()=>
        
            igeneric.LoadData<Books, dynamic>("SELECT * FROM dbo.Books", new { });
        

        public async Task<Books> GetBookById(int id)
        {
            var res = await igeneric.LoadData<Books, dynamic>("SELECT * FROM dbo.Books WHERE BookId = @id", new { id });
            return res.FirstOrDefault();
        }

        public async Task<bool> UpdateBook(Books book)
        {

            await igeneric.SaveData<Books, dynamic>("UPDATE dbo.Books SET BookName=@BookName WHERE BookId=@BookId",new { book.BookName, book.BookId });
            return true;
        }
    }
}
