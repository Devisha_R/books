﻿using Dapper;
using DataAccess.Interface;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class GenericRepo : IGeneric
    {
        private IOptions<ConnectionStrings> _config;

        public GenericRepo(IOptions<ConnectionStrings> config)
        {
            _config = config;
        }

        public async Task<IEnumerable<T>> LoadData<T, U>(string sqlQuery, U parameters, string connectionId = "default")
        {
            IDbConnection connection = new SqlConnection(_config.Value.DataConnection);
            return await connection.QueryAsync<T>(sqlQuery, parameters, commandType: CommandType.Text);
        }

        public async Task SaveData<T, U>(string sqlquery, U parameters, string connectionid = "default")
        {
            IDbConnection conn = new SqlConnection(_config.Value.DataConnection);
            await conn.ExecuteAsync(sqlquery, parameters, commandType: CommandType.Text);
        }
    }
}
