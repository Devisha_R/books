﻿using DataAccess.Interface;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class UserRepo : IUser
    {
        private readonly IGeneric igeneric;

        public UserRepo(IGeneric igeneric)
        {
            this.igeneric = igeneric;
        }

        public async Task<bool> AddUser(User user)
        {
            await igeneric.SaveData<User, dynamic>("INSERT INTO [dbo].[User] (UserId,UserName) VALUES (@UserId,@UserName)",
            new { user.UserId, user.UserName });
            return true;
        }

        public async Task<bool> DeleteUser(int id)
        {
            await igeneric.SaveData<User, dynamic>("DELETE FROM [dbo].[User] WHERE UserId=@id", new { id });
            return true;
        }

        public async Task<IEnumerable<User>> GetAllUser() =>

            await igeneric.LoadData<User, dynamic>("SELECT * FROM [dbo].[User]", new { });


        public async Task<User> GetUserById(int id)
        {
            var res = await igeneric.LoadData<User, dynamic>("SELECT * FROM [dbo].[User] WHERE UserId=@id", new {id });
            return res.FirstOrDefault();
        }

        public async Task<bool> UpdateUser(User user)
        {
            await igeneric.SaveData<User, dynamic>("UPDATE [dbo].[User] SET UserName=@username WHERE UserId=@userid",
            new {user.UserName, user.UserId });
            return true;
        }
    }
}
