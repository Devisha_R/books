﻿using CQRS.Commands;
using DataAccess.Interface;
using Domain.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS.Handlers
{
    public class AddBooksHandler : IRequestHandler<AddBooksCommand, Books>
    {
        private readonly IBooks books;

        public AddBooksHandler(IBooks books)
        {
            this.books = books;
        }

        public async Task<Books> Handle(AddBooksCommand request, CancellationToken cancellationToken)
        {
            return await(books.AddBook(request.book));
        }
    }
}
