﻿using CQRS.Queries;
using DataAccess.Interface;
using Domain.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS.Handlers
{
    public class GetBooksByIdHandler : IRequestHandler<GetBookByIdQuery, Books>
    {
        private readonly IBooks books;

        public GetBooksByIdHandler(IBooks books)
        {
            this.books = books;
        }

        public async Task<Books> Handle(GetBookByIdQuery request, CancellationToken cancellationToken)
        {
            return await(books.GetBookById(request.id));
        }
    }
}
