﻿using CQRS.Queries;
using DataAccess.Interface;
using Domain.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS.Handlers
{
    public class GetAllBooksHandler : IRequestHandler<GetAllBooksQuery, IEnumerable<Books>>
    {
        private readonly IBooks books;

        public GetAllBooksHandler(IBooks books)
        {
            this.books = books;
        }

        public async Task<IEnumerable<Books>> Handle(GetAllBooksQuery request, CancellationToken cancellationToken)
        {
            return await (books.GetAllBooks());
        }
    }
}
