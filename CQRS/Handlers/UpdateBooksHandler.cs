﻿using CQRS.Commands;
using CQRS.Queries;
using DataAccess.Interface;
using Domain.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS.Handlers
{
    public class UpdateBooksHandler : IRequestHandler<UpdateBookCommand, bool>
    {
        private readonly IBooks books;

        public UpdateBooksHandler(IBooks books)
        {
            this.books = books;
        }

        public async Task<bool> Handle(UpdateBookCommand request, CancellationToken cancellationToken)
        {
            return await (books.UpdateBook(request.book));
        }
    }
}
