﻿using CQRS.Queries;
using DataAccess.Interface;
using Domain.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS.Handlers
{
    public class GetUsersByIdHandler : IRequestHandler<GetUserByIdQuery, User>
    {
        private readonly IUser user;

        public GetUsersByIdHandler(IUser user)
        {
            this.user = user;
        }

        public async Task<User> Handle(GetUserByIdQuery request, CancellationToken cancellationToken)
        {
            return await (user.GetUserById(request.UserId));
        }
    }
}
