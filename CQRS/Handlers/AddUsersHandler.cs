﻿using CQRS.Commands;
using DataAccess.Interface;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS.Handlers
{
    public class AddUsersHandler : IRequestHandler<AddUserCommand, bool>
    {
        private readonly IUser user;

        public AddUsersHandler(IUser user)
        {
            this.user = user;
        }

        public async Task<bool> Handle(AddUserCommand request, CancellationToken cancellationToken)
        {
            return await (user.AddUser(request.user));
        }
    }
}
