﻿using CQRS.Commands;
using DataAccess.Interface;
using Domain.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS.Handlers
{
    public class DeleteUsersHandler : IRequestHandler<DeleteUserCommand, bool>
    {
        private readonly IUser user;

        public DeleteUsersHandler(IUser user)
        {
            this.user = user;
        }

        public async Task<bool> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            return await(user.DeleteUser(request.userid));
        }
    }
}
