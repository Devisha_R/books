﻿using CQRS.Commands;
using DataAccess.Interface;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS.Handlers
{
    public class UpdateUsersHandler : IRequestHandler<UpdateUserCommand, bool>
    {
        private readonly IUser user;

        public UpdateUsersHandler(IUser user)
        {
            this.user = user;
        }

        public async Task<bool> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            return await (user.UpdateUser(request.user));
        }
    }
}
