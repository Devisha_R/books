﻿using CQRS.Commands;
using DataAccess.Interface;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS.Handlers
{
    public class DeleteBooksHandler : IRequestHandler<DeleteBookCommand, bool>
    {
        private readonly IBooks books;

        public DeleteBooksHandler(IBooks books)
        {
            this.books = books;
        }

        public async Task<bool> Handle(DeleteBookCommand request, CancellationToken cancellationToken)
        {
            return await(books.DeleteBook(request.bookid));
        }
    }
}
